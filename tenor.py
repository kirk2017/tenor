from gurobipy import *


V_S, a, b, c = multidict(
    {
        1: [14, 16, 1000],
        2: [8, 20, 2000],
        3: [12, 15, 500],
        4: [14, 24, 8000],
        5: [10, 25, 2000],
        6: [12, 15, 1000],
        7: [6, 12, 2000]
    }
)

A_S, d, e = multidict(
    {
        (1, 2): [1000, 0.05],
        (2, 1): [1000, 0.05],
        (3, 1): [2000, 0.08],
        (1, 3): [2000, 0.08],
        (2, 3): [1500, 0.06],
        (3, 2): [1500, 0.06],
        (2, 4): [1500, 0.04],
        (4, 2): [1500, 0.04],
        (3, 4): [2000, 0.06],
        (4, 3): [2000, 0.06],
        (5, 3): [1500, 0.02],
        (3, 5): [1500, 0.02],
        (6, 3): [1500, 0.07],
        (3, 6): [1500, 0.07],
        (5, 6): [1200, 0.045],
        (6, 5): [1200, 0.045],
        (6, 7): [1500, 0.03],
        (7, 6): [1500, 0.03],
    }
)

V_ID, c = multidict(
    {
        1: 0.4,
        2: 0.5,
        3: 0.8,
        4: 0.2,
        5: 0.3,
        6: 0.4,
        7: 0.32,
    }
)


S = {
    1: [multidict({
        1: ['t1', 4, 1, 10],
        2: ['t2', 3, 2, 40],
        3: ['t3', 3, 2, 30],
        4: ['t4', 6, 1, 50]
    }, ),
        multidict({
            (1, 2): 500,
            (2, 3): 400,
            (3, 4): 400
        })],

    2: [multidict({
        1: ['t1', 4, 1, 10],
        2: ['t2', 3, 2, 40],
        3: ['t3', 3, 2, 30],
        4: ['t4', 6, 1, 50]
    }, ),
        multidict({
            (1, 2): 500,
            (2, 3): 400,
            (3, 4): 400
        })]
}


try:
    tenor = Model("tenor")

    # decision variable y
    for s in S:
        # S[s].__getitem__(0)[0] == V_I
        y = tenor.addVars(s, S[s].__getitem__(0)[0], V_S,
                          vtype=GRB.BINARY,
                          name="y")
    # decision variable x
    for s in S:
        # S[s].__getitem__(1)[0] == A_I
        x = tenor.addVars(s, S[s].__getitem__(1)[0], A_S,
                          vtype=GRB.BINARY,
                          name="x")
    # constraint 2
    tenor.addConstrs(
        (quicksum(y[s - 1, i, j] for j in V_S) == 1 for s in S for i in S[s].__getitem__(0)[0]),
        "c2")

    # constraint 3
    tenor.addConstrs(
        (
            quicksum(x[s - 1, h, k, p, q]) - quicksum(x[s - 1, h, k, q, p]) ==
            y[s - 1, h, p] - y[s - 1, k, p] for (h, k) in A_S for p in S[s].__getitem__(0)[0]),
        "c3")

    tenor.update()

    # the objective function
    obj = quicksum(
        quicksum(y[s - 1, i, j] * c[j] for i in S[s].__getitem__(0)[0] for j in V_S)
        + quicksum(x[s - 1, i, j, u, v] * 2.0 * 1.0 for i, j in S[s][1][0] for u, v in A_S)
        for s in S)

    tenor.setObjective(obj, sense=GRB.MAXIMIZE)

    tenor.optimize()

    tenor.printAttr('X')

    tenor.write('tenor.lp')
    tenor.write('tenor.mps')
    tenor.write('tenor.sol')

except GurobiError:
    print 'Error reported'
